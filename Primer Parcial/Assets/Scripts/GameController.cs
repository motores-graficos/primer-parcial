using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public GameObject player;
    public GameObject bot;
    private List<GameObject> enemyList = new List<GameObject>();
    float timeLeft;

    public TMPro.TMP_Text gameOverText;
    public TMPro.TMP_Text timerText;

    void Start()
    {
        GameStart();
        gameOverText.text = "";
        timerText.text = "";
    }

    void Update()
    {
        if (timeLeft == 0)
        {
            GameStart();
        }
    }

    void GameStart()
    {

        foreach (GameObject item in enemyList)
        {
            Destroy(item);
        }

        enemyList.Add(Instantiate(bot, new Vector3(5, 1f, 3f), Quaternion.identity));
        enemyList.Add(Instantiate(bot, new Vector3(3, 1f, 3f), Quaternion.identity));
        enemyList.Add(Instantiate(bot, new Vector3(1, 1f, 3f), Quaternion.identity));
        StartCoroutine(TimeStart(60));
    }

    public IEnumerator TimeStart(float timeValue = 60)
    {
        timeLeft = timeValue;
        while (timeLeft > 0)
        {
            timerText.text = $"Time left: " + timeLeft + " s.";
            yield return new WaitForSeconds(1.0f);
            timeLeft--;
        }
        if (timeLeft == 0)
        {
            gameOverText.text = "Game Over";
            yield return new WaitForSeconds(2.5f);
            Debug.Log("Game Over");
            ReloadLevel();
        }
    }

    // Level Reload
    private void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
