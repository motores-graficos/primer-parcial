using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    // Mouse controller
    Vector2 mouseLook;
    Vector2 softenessV;
    public float sensitivity = 5.0f;
    public float softened = 2.0f;

    GameObject player;
    void Start()
    {
        player = this.transform.parent.gameObject;
    }

    void Update()
    {
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        md = Vector2.Scale(md, new Vector2(sensitivity * softened, sensitivity * softened));

        softenessV.x = Mathf.Lerp(softenessV.x, md.x, 1f / softened);
        softenessV.y = Mathf.Lerp(softenessV.y, md.y, 1f / softened);

        mouseLook += softenessV;
        mouseLook.y = Mathf.Clamp(mouseLook.y, -90f, 90f);
        transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
        player.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, player.transform.up);
    }
}
