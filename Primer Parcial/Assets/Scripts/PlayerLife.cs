using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerLife : MonoBehaviour
{
    public float threshold;
    public TMPro.TMP_Text youDiedText;

    private void Start()
    {
        youDiedText.text = "";
    }

    private void Update()
    {
        FallOfScene();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("movingEnemy"))
        {
            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<Rigidbody>().isKinematic = true;
            GetComponent<PlayerController>().enabled = false;
            Die();
        }

        if (collision.gameObject.CompareTag("Chasing Enemy"))
        {
            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<Rigidbody>().isKinematic = true;
            GetComponent<PlayerController>().enabled = false;
            Die();
        }

        if (collision.gameObject.CompareTag("bullet"))
        {
            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<Rigidbody>().isKinematic = true;
            GetComponent<PlayerController>().enabled = false;
            Die();
        }
    }

    private void Die()
    {
        youDiedText.text = "You died!";
        Invoke(nameof(ReloadLevel), 3f);
    }

    private void FallOfScene()
    {
        if (transform.position.y < threshold)
        {
            ReloadLevel();
        }
    }

    private void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
