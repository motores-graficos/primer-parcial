using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    public Transform player;

    // Bullet information
    public float range = 50.0f;
    public float bulletImpulse = 25.0f;

    private int hp;
    public int speed;

    // Range 
    private bool onRange = false;

    // Projectile
    public Rigidbody projectile;

    void Start()
    {
        // Random range of bullet
        float rand = Random.Range(1.0f, 2.0f);
        InvokeRepeating("Shoot", 1, rand);

        // Boss life count
        hp = 2500;
    }

    void Update()
    {
        onRange = Vector3.Distance(transform.position, player.position) < range;

        if (onRange)
        {
            transform.LookAt(player);
        }
    }

    // Shooting
    void Shoot()
    {
        // Shoot if player is on range
        if (onRange)
        {
            Rigidbody bullet = (Rigidbody)Instantiate(projectile, transform.position + transform.forward, transform.rotation);
            bullet.AddForce(transform.forward * bulletImpulse, ForceMode.Impulse);

            Destroy(bullet.gameObject, 2);
        }
    }

    // Taking players bullet damage
    public void takeDamage()
    {
        hp = hp - 25;

        if (hp <= 0)
        {
            this.disappear();
        }
    }

    // Disappear object
    private void disappear()
    {
        Destroy(gameObject);
    }

    // Check if collides with a bullet
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bullet"))
        {
            takeDamage();
        }
    }
}
