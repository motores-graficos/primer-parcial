using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotController : MonoBehaviour
{
    private int hp;
    private GameObject player;
    public int speed;

    void Start()
    {
        hp = 100;
        player = GameObject.Find("Player");
    }

    private void Update()
    {
        transform.LookAt(player.transform);
        transform.Translate(speed * Vector3.forward * Time.deltaTime);
    }

    public void takeDamage()
    {
        hp = hp - 25;

        if (hp <= 0)
        {
            this.disappear();
        }
    }

    private void disappear()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bullet"))
        {
            takeDamage();
        }
    }
}
