using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    // Components
    Rigidbody rb;

    // Movement
    public float movingSpeed = 5f;

    // Camera
    public Camera firstPersonCamera;

    // Projectile
    public GameObject projectile;

    // Jump
    public LayerMask floorLayer;
    public float jumpMagnitude;
    public CapsuleCollider col;
    public float jumpForce = 5f;
    public float groundDistance = 0.5f;
    bool canDoubleJump;

    // UI
    public TMPro.TMP_Text amountCollectedText;
    public TMPro.TMP_Text youWonText;
    private int count;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        youWonText.text = "";
        textSetting();
    }

    void Update()
    {
        MouseUnlock();
        Movement();
        Shooting();
        Jumping();
        Restart();
    }

    // Mouse unlock when pressing "Esc"
    private void MouseUnlock()
    {
        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }

    // Movement
    private void Movement()
    {
        float verticalMovement = Input.GetAxis("Vertical") * movingSpeed;
        float horizontalMovement = Input.GetAxis("Horizontal") * movingSpeed;

        verticalMovement *= Time.deltaTime;
        horizontalMovement *= Time.deltaTime;

        transform.Translate(horizontalMovement, 0, verticalMovement);
    }

    // Shooting
    private void Shooting()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = firstPersonCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            GameObject proj;
            proj = Instantiate(projectile, ray.origin, transform.rotation);

            Rigidbody rb = proj.GetComponent<Rigidbody>();
            rb.AddForce(firstPersonCamera.transform.forward * 25, ForceMode.Impulse);

            Destroy(proj, 3);

            // Checking if Raycast hits an object
            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
            {
                Debug.Log("Ray hit " + hit.collider.name);

                // Damages enemy
                if (hit.collider.name.Substring(0, 3) == "Bot")
                {
                    GameObject objectHit = GameObject.Find(hit.transform.name);
                    BotController scriptObjectHit = (BotController)objectHit.GetComponent(typeof(BotController));

                    if (scriptObjectHit != null)
                    {
                        scriptObjectHit.takeDamage();
                    }
                }
            }
        }
    }

    // Checking if player collides with Moving Bot
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy Head"))
        {
            Destroy(collision.transform.parent.gameObject);
        }
    }

    // Restart scene when player presses "R"
    private void Restart()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            ReloadLevel();
        }
    }

    // Jumping
    private void Jumping()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (IsGrounded())
            {
                rb.velocity = Vector3.up * jumpForce;
                canDoubleJump = true;
            }
            else if (canDoubleJump)
            {
                rb.velocity = Vector3.up * jumpForce;
                canDoubleJump = false;
            }
        }
    }

    // Checking if player is on the ground
    private bool IsGrounded()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * 0.9f, floorLayer);
    }

    // Settting player collectibles UI.
    private void textSetting()
    {
        amountCollectedText.text = "Golden boards: " + count.ToString();
        if (count >= 4)
        {
            youWonText.text = "All Golden Boards have been collected. Congratulations, you won!";
        }
    }

    // Checking if player collides with collectible and adding +1 to Collectible Text
    private void OnTriggerEnter(Collider other)
    {
        // Checking if player collides with collectible and adding +1 to Collectible Text
        if (other.gameObject.CompareTag("collectible") == true)
        {
            count = count + 1;
            textSetting();
            other.gameObject.SetActive(false);
        }

        // Checking if player collides with collectible and increases moving speed
        if (other.gameObject.CompareTag("Speed Capsule") == true)
        {
            movingSpeed += 7f;
            other.gameObject.SetActive(false);
        }

        // Checking if player collides with collectible and increases player scale
        if (other.gameObject.CompareTag("Size Capsule") == true)
        {
            transform.localScale += new Vector3(1.5f, 1.5f, 1.5f);

            // Change ray shooting position
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = firstPersonCamera.ViewportPointToRay(new Vector3(0.7f, 0.7f, 0.7f));
                RaycastHit hit;

                GameObject proj;
                proj = Instantiate(projectile, ray.origin, transform.rotation);

                Rigidbody rb = proj.GetComponent<Rigidbody>();
                rb.AddForce(firstPersonCamera.transform.forward * 25, ForceMode.Impulse);

                Destroy(proj, 3);

                // Checking if Raycast hits an object
                if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
                {
                    Debug.Log("Ray hit " + hit.collider.name);

                    // Damages enemy
                    if (hit.collider.name.Substring(0, 3) == "Bot")
                    {
                        GameObject objectHit = GameObject.Find(hit.transform.name);
                        BotController scriptObjectHit = (BotController)objectHit.GetComponent(typeof(BotController));

                        if (scriptObjectHit != null)
                        {
                            scriptObjectHit.takeDamage();
                        }
                    }
                }
            }

            jumpForce += 7f;

            other.gameObject.SetActive(false);
        }
    }

    // Level Reload
    private void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
